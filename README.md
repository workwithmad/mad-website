Hey there!

We hope you enjoy going through the Mäd website as much as we did making it! Mäd is always changing and evolving, and we are proud to put into this repo the third iteration of our website.

Whether you are a young web developer, a marketing professional looking for a good launch pad for a new website, or just curious, I hope you can find this repo useful for your purpose.

Please let us know your feedback, and if you've created something using this repo, let us know too! We'd love to hear your success story.

Cheers,

Emannuele Faja
CEO